#version 120

uniform samplerCube s3d_cubemap_texture;

varying vec3 frag_cube_uv;

void main () {
	gl_FragColor = textureCube(s3d_cubemap_texture, frag_cube_uv);
}
