#version 120
uniform mat4 s3d_camera_proj;
uniform mat4 s3d_camera_view;
uniform mat4 s3d_model_transform;

attribute vec3 s3d_geometry_vertex;
void main() {
    gl_Position = s3d_camera_proj * s3d_camera_view * s3d_model_transform * vec4(s3d_geometry_vertex, 1.0);
}
