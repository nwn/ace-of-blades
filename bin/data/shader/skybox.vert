#version 120

uniform mat4 s3d_camera_proj;
uniform mat4 s3d_camera_orientation;

attribute vec3 s3d_geometry_vertex;

varying vec3 frag_cube_uv;

void main() {
	frag_cube_uv = s3d_geometry_vertex;
	gl_Position = s3d_camera_proj * s3d_camera_orientation * vec4 (s3d_geometry_vertex, 1.0);
}
