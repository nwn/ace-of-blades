#ifndef AOB_SERVER_H
#define AOB_SERVER_H

#include <SFML/Network.hpp>
#include <iostream>

class Server {
public:
    Server();
    const bool bindToPort(const unsigned short port);
    void unbind();
    unsigned short getPort();

    void setTickrate(const float tickrate);
    const float getTickrate();

    void setTimeout(const float timeout);
    const float getTimeout();

    void update();
protected:
    virtual void updateWorld(const float dts) = 0;
    virtual void sendWorld() = 0;
    virtual void handlePacket(sf::Packet& foreignPacket, sf::IpAddress& foreignAddress, const unsigned short foreignPort) = 0;

    sf::UdpSocket* socket();
private:
    sf::UdpSocket mSocket;
    sf::Clock mTickrateTimer;
    float mTickrate;    // ms
    float mTimeout;     // ms
};

#endif // AOB_SERVER_H
