#include "server.h"

// public
Server::Server() {
    socket()->setBlocking(false);
    setTickrate(10);
    setTimeout(1000);
}
const bool Server::bindToPort(const unsigned short port) {
    unbind();
    if(socket()->bind(port) == sf::Socket::Status::Done) {
        return true;
    }
    return false;
}
void Server::unbind() {
    socket()->unbind();
}
unsigned short Server::getPort() {
    return socket()->getLocalPort();
}
void Server::setTickrate(const float tickrate) {
    if(tickrate > 0) {
        if(getTickrate() != tickrate) {
            mTickrate = tickrate;
        }
    }
}
const float Server::getTickrate() {
    return mTickrate;
}

void Server::setTimeout(const float timeout) {
    if(timeout > 0) {
        if(getTimeout() != timeout) {
            mTimeout = timeout;
        }
    }
}
const float Server::getTimeout() {
    return mTimeout;
}

void Server::update() {
    { // update and send
        if(mTickrateTimer.getElapsedTime().asMilliseconds() >= getTickrate()) {
            mTickrateTimer.restart();
            // update world
            updateWorld(getTickrate() / 1000.f);
            // send world packet
            sendWorld();
        }
    }
    { // receive
        sf::Packet     foreignPacket;
        sf::IpAddress  foreignAddress;
        unsigned short foreignPort;

        sf::Socket::Status status = mSocket.receive(foreignPacket, foreignAddress, foreignPort);
        if(status == sf::Socket::Status::Done) {
            // handle received data
            handlePacket(foreignPacket, foreignAddress, foreignPort);
        } else {
            if(status == sf::Socket::Status::Error) {
                throw std::runtime_error("sf::Socket::Status::Error");
            }
        }
    }
}

// protected
sf::UdpSocket* Server::socket() {
    return &mSocket;
}
