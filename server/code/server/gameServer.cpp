#include "gameServer.h"

GameServer::GameServer() {
    setTickrate(16);    // 64 times / second
    setTimeout(5000);
    setConsole(nullptr);
}
GameServer::~GameServer() {
    disconnect();
}

void GameServer::disconnect(net::ServerPacket::Disconnect data) {
    if(mClients.size() > 0) {
        net::ServerPacket packet;
        packet.mType = net::ServerPacket::Type::Disconnect;
        packet.mDisconnect = data;
        sendAll(packet);
        removeAll();
    }
}

const bool GameServer::hasPlayer(ClientID& id) {
    return (getPlayer(id) != nullptr);
}
game::Player* GameServer::getPlayer(ClientID& id) {
    if(getClient(id) != nullptr) {
        return getClient(id)->mPlayer;
    }
    return nullptr;
}

void GameServer::setConsole(Console* console) {
    mConsole = console;
}
Console* GameServer::getConsole() {
    return mConsole;
}
const bool GameServer::hasConsole() {
    return (mConsole != nullptr);
}

ClientID& GameServer::getSvClient() {
    return mCommandClient;
}
game::State* GameServer::state() {
    return &mState;
}

void GameServer::updateWorld(const float dts) {
    // maybe update the clients non-movement and non-time sensitive requests
    mState.update(dts);
}
void GameServer::sendWorld() {
    if(mClients.size() > 0) {
        // check for any lost connections before sending update packet
        for(unsigned int i=0;i<mClients.size();i++) {
            if(mClients.at(i).mClock.getElapsedTime().asMilliseconds() >= getTimeout()) {
                std::cout << getClientInfo(mClients.at(i).mID) << " timeout\n";
                removeClient(mClients.at(i).mID);
            }
        }

        net::ServerPacket packet;
        packet.mType = net::ServerPacket::Type::Update;
        // send a copy of the game state
        packet.mUpdate.mState.set(mState);
        sendAll(packet);

        state()->clearEvents();
    }
}
void GameServer::handlePacket(sf::Packet& foreignPacket, sf::IpAddress& foreignAddress, const unsigned short foreignPort) {
    ClientID id;
    id.mAddress = foreignAddress;
    id.mPort = foreignPort;

    net::ClientPacket request;
    foreignPacket >> request;

    if(request.mType == net::ClientPacket::Type::Connect) {
        if(!clientExists(id)) {
            // add client with his requested name
            ClientData* newClient = addClient(id, request.mConnect.mName);

            // send him his real name stored on server
            net::ServerPacket response;
            response.mType = net::ServerPacket::Type::Connect;
            response.mConnect.mName = newClient->mPlayer->getName();
            sendClient(newClient->mID, response);

            std::cout << getClientInfo(newClient->mID) + " connected\n";
        }
    } else if(request.mType == net::ClientPacket::Type::Update) {
        if(clientExists(id)) {
            ClientData* client = getClient(id);
            client->mClock.restart();

            // specific, time-sensitive requests
            if(!request.mUpdate.mName.empty()) {    // name
                setClientName(id, request.mUpdate.mName);
            } else if(!request.mUpdate.mCommand.empty()) {    // command
                executeClientCommand(id, request.mUpdate.mCommand);
            } else {
                // general requests
                mState.getPlayer(getClient(id)->mPlayer->getName())->requests()->set(request.mUpdate.mRequests);
            }
        }
    } else if(request.mType == net::ClientPacket::Type::Disconnect) {
        if(clientExists(id)) {
            std::cout << getClientInfo(id) + " disconnected: " + request.mDisconnect.mReason + "\n";
            removeClient(id);
        }
    }
}

void GameServer::sendAll(net::ServerPacket& svPacket) {
    for(unsigned int i=0;i<mClients.size();i++) {
        sendClient(mClients.at(i).mID, svPacket);
    }
}
void GameServer::sendClient(ClientID& id, net::ServerPacket& svPacket) {
    sf::Packet packet;
    packet << svPacket;
    socket()->send(packet, id.mAddress, id.mPort);
}

ClientData* GameServer::getClient(ClientID& id) {
    for(unsigned int i=0;i<mClients.size();i++) {
        if(id.mAddress == mClients.at(i).mID.mAddress && id.mPort == mClients.at(i).mID.mPort) {
            return &mClients.at(i);
        }
    }
    return nullptr;
}

const bool GameServer::clientExists(ClientID& id) {
    return (getClient(id) != nullptr);
}
ClientData* GameServer::addClient(ClientID& id, const std::string& name) {
    mClients.emplace_back();
    mClients.back().mID = id;
    mClients.back().mPlayer = mState.addPlayer(name);
    return &mClients.back();
}
void GameServer::removeAll() {
    for(unsigned int i=0;i<mClients.size();i++) {
        removeClient(mClients.at(i).mID);
    }
    // remove all players from the world
    mState.removeAllPlayers();
}
void GameServer::removeClient(ClientID& id) {
    for(unsigned int i=0;i<mClients.size();i++) {
        if(id.mAddress == mClients.at(i).mID.mAddress && id.mPort == mClients.at(i).mID.mPort) {
            // add him into the world
            mState.removePlayer(mClients.at(i).mPlayer->getName());

            mClients.at(i) = mClients.back();
            mClients.pop_back();
        }
    }
}

const std::string GameServer::getClientInfo(ClientID& id) {
    ClientData* data = getClient(id);
    if(data != nullptr) {
        return data->mPlayer->getName() + " [" + data->mID.mAddress.toString() + ":" + s3d::util::toString(data->mID.mPort) + "]";
    } else {
        return "unknown";
    }
}

void GameServer::setClientName(ClientID& id, const std::string& name) {
    const std::string oldInfo = getClientInfo(id);
    const std::string oldName = getClient(id)->mPlayer->getName();

    const std::vector<std::string> tokens = Console::tokenize(name);
    if(tokens.size() > 0 && tokens.at(0) != oldName) {
        // change his name now
        mState.getPlayer(oldName)->setName(mState.generateName(name));
        const std::string newName = getClient(id)->mPlayer->getName();
        if(newName != oldName) {
            // send the clients new name back
            net::ServerPacket packet;
            packet.mType = net::ServerPacket::Type::Update;
            packet.mUpdate.mName = newName;
            sendClient(id, packet);

            std::cout << oldInfo << " set name to '" << newName << "'\n";
        }
    }
}
void GameServer::executeClientCommand(ClientID& id, const std::string& command) {
    // execute his command here
    if(hasConsole()) {
        mCommandClient = id;
        getConsole()->inputString(command);
        getConsole()->execute();
        // send back a response
        std::vector<std::string> outputs = getConsole()->getLastOutput(command);
        std::string response;
        for(unsigned int i=0;i<outputs.size();i++) {
            response.append(outputs.at(i));
            if(i < outputs.size()-1) {
                response += '\n';
            }
        }
        net::ServerPacket packet;
        packet.mType = net::ServerPacket::Type::Update;
        if(response.empty()) {
            response = "executed '" + command + "'";
        }
        packet.mUpdate.mResponse = response;
        sendClient(id, packet);
    }
    std::cout << getClientInfo(id) + " executed '" << command << "'\n";
}
