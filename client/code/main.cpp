#include "world/world.h"
#include "console/s3dconsole.h"
#include "client/gameClient.h"
#include "player/input.h"

#include <s3d/window.h>
#include <s3d/util.h>
#include <SFML/Network.hpp>

s3d::Window gWindow;
GameClient gClient;
s3d::Camera gCamera;
bool gRunning = true;

void exit(const std::string& args, Console* console) {
    gRunning = false;
}
void fov(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float fov = Console::Variable::convertFromString<float>(tokens.at(0));
        gCamera.setFieldOfView(fov);
        console->print("successfully set fov to " + Console::Variable::convertToString(gCamera.getFieldOfView()));
    } else {
        console->print("fov set to " + Console::Variable::convertToString(gCamera.getFieldOfView()));
    }
}
void bind(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = s3d::Console::tokenize(args);
    if(tokens.size() == 1) {
        const unsigned short port = s3d::Console::Variable::convertFromString<unsigned short>(tokens.at(0));
        const unsigned short lastPort = gClient.getPort();
        if(gClient.bindToPort(port)) {
            console->print("successfully bind to port " + s3d::util::toString(port));
        } else {
            console->print("failed to bind to port " + s3d::util::toString(port));
            gClient.bindToPort(lastPort);
        }
    } else {
        console->print("currently bound to " + Console::Variable::convertToString(gClient.getPort()));
    }
}
void connect(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = s3d::Console::tokenize(args);
    if(tokens.size() == 2) {
        const std::string ip = tokens.at(0);
        const unsigned short port = s3d::Console::Variable::convertFromString<unsigned short>(tokens.at(1));
        if(port > 0) {
            gClient.setServerAddress(ip);
            gClient.setServerPort(port);
            gClient.connect();
        } else {
            console->print("invalid port '" + tokens.at(1) + "'");
        }
    } else {
        console->print("usage: connect [ip port]");
    }
}
void disconnect(const std::string& args, Console* console) {
    if(gClient.connected()) {
        gClient.disconnect();
        gCamera.transform()->setOrientation(glm::quat());
        console->print("disconnected successfully");
    } else {
        console->print("no server to disconnect from");
    }
}
void tickrate(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float tickrate = Console::Variable::convertFromString<float>(tokens.at(0));
        gClient.setTickrate(tickrate);
        console->print("successfully set tickrate to " + Console::Variable::convertToString(gClient.getTickrate()));
    } else {
        console->print("tickrate set to " + Console::Variable::convertToString(gClient.getTickrate()));
    }
}
void timeout(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float timeout = Console::Variable::convertFromString<float>(tokens.at(0));
        gClient.setTimeout(timeout);
        console->print("successfully set timeout to " + Console::Variable::convertToString(gClient.getTimeout()));
    } else {
        console->print("timeout set to " + Console::Variable::convertToString(gClient.getTimeout()));
    }
}
void sv(const std::string& args, Console* console) {
    if(gClient.connected()) {
        const std::vector<std::string> tokens = s3d::Console::tokenize(args);
        if(tokens.size() > 0) {
            net::ClientPacket packet;
            packet.mType = net::ClientPacket::Type::Update;
            packet.mUpdate.mCommand = args;
            gClient.send(packet);
        } else {
            console->print("usage: sv [command]");
        }
    } else {
        console->print("you are not connected to a server, use 'connect [ip port]'");
    }
}
void setName(const std::string& args, Console* console) {
    if(gClient.connected()) {
        const std::vector<std::string> tokens = s3d::Console::tokenize(args);
        if(tokens.size() > 0) {
            net::ClientPacket packet;
            packet.mType = net::ClientPacket::Type::Update;
            packet.mUpdate.mName = args;
            gClient.send(packet);
        } else {
            if(gClient.hasWorld() && gClient.getWorld()->hasLocalPlayer()) {
                console->print("name is set to '" + gClient.getWorld()->getLocalPlayer()->getName() + "'");
            }
        }
    } else {
        console->print("you are not connected to a server, use 'connect [ip port]'");
    }
}
void sens(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = s3d::Console::tokenize(args);
    if(tokens.size() >= 2) {
        const float sensX = Console::Variable::convertFromString<float>(tokens.at(0));
        const float sensY = Console::Variable::convertFromString<float>(tokens.at(1));
        gCamera.setSensitivity(sensX, sensY);
    } else {
        console->print("sensitivity is set to " +
                       Console::Variable::convertToString(gCamera.getSensitivityX()) + " " +
                       Console::Variable::convertToString(gCamera.getSensitivityY()));
    }
}

int contextDependent() {
    try {
        srand(time(nullptr));

        // Opengl context
        sf::ContextSettings settings;
        settings.depthBits          = 16;
        settings.stencilBits        = 16;
        settings.antialiasingLevel  = 8;
        settings.majorVersion       = 2;
        settings.minorVersion       = 1;
        gWindow.get()->create(sf::VideoMode(800, 600), "Ace of Blades", sf::Style::Close, settings);
        gWindow.initGLEW();

        // UI
        sf::Font pixelUnicode;
        if(!pixelUnicode.loadFromFile("data/font/Pixel-UniCode.ttf")) {
            throw std::runtime_error("failed to load font");
        }
        sf::Font bit8Operator;
        if(!bit8Operator.loadFromFile("data/font/8bitoperator.ttf")) {
            throw std::runtime_error("failed to load font");
        }

        // FPS
        sf::Text tfps;
        tfps.setFont(pixelUnicode);
        tfps.setCharacterSize(16);
        tfps.setString("fps: 60");
        tfps.setPosition(sf::Vector2f(1, -4));

        // Console
        s3d::Console console;
        console.setInput("connect 127.0.0.1 8008");
        console.setVisible(false);
        console.input()->setEnabled(console.isVisible());

        console.setInputFont(&pixelUnicode);
        console.setOutputFont(&pixelUnicode);

        console.setInputSize(16);
        console.setOutputSize(16);

        console.setInputColor(sf::Color(230, 230, 230));
        console.setOutputColor(sf::Color(230, 230, 230));

        console.setOutputLimit(100);
        console.setOutputShow(23);
        console.setHistoryLimit(23);

        console.addFunction("sens", "sets your mouse sensitivity", "x y", &sens);
        console.addFunction("name", "sets your alias", "name", &setName);
        console.addFunction("fov", "sets camera's fov", "fov", &fov);

        console.addFunction("tickrate", "sets the tickrate", "ms", &tickrate);
        console.addFunction("timeout", "sets the timeout", "ms", &timeout);

        console.addFunction("connect", "connects to a server", "ip port", &connect);
        console.addFunction("disconnect", "disconnects from", "", &disconnect);
        console.addFunction("sv", "sends a command to the server", "command", &sv);

        console.addFunction("bind", "binds to a port", "port", &bind);
        console.addFunction("exit", "quits the program", "", &exit);

        // 3D
        s3d::Scene scene;
        gCamera.setSensitivity(0.1, 0.1);
        gCamera.setClearColor(glm::vec3(0.1));
        gCamera.setFieldOfView(90);
        gCamera.setAspectRatio((float)gWindow.get()->getSize().x / (float)gWindow.get()->getSize().y);
        gCamera.setNearFar(0.1, 1000);
        gCamera.transform()->setPosition(glm::vec3(0, 0, 3));
        scene.setCamera(&gCamera);

        // S3D
        s3d::Geometry gizmo;
        gizmo.loadObj("data/mesh/gizmo.obj");
        gizmo.upload();

        s3d::Geometry roomGeometry;
        roomGeometry.loadObj("data/mesh/room.obj");
        roomGeometry.upload();

        s3d::Texture gizmoTexture;
        gizmoTexture.loadFromFile("data/texture/gizmo.png");

        s3d::Geometry playerGeometry;
        playerGeometry.loadObj("data/mesh/player.obj");
        playerGeometry.upload();

        s3d::Texture playerTexture;
        playerTexture.loadFromFile("data/texture/player.png");
        playerTexture.setFilteringNearest();
        playerTexture.setWrappingClamp();

        s3d::Texture roomTexture;
        roomTexture.loadFromFile("data/texture/room.png");
        roomTexture.addAnisotropy();
        roomTexture.setFilteringNearest();
        roomTexture.setWrappingRepeat();

        s3d::Cubemap sky;
        sky.loadFromFiles("data/texture/sky/right.jpg", "data/texture/sky/left.jpg",
                          "data/texture/sky/up.jpg", "data/texture/sky/down.jpg",
                          "data/texture/sky/back.jpg", "data/texture/sky/front.jpg");

        s3d::Shader textureShader;
        textureShader.loadFromFile("data/shader/static_model_diffuse.vert", "data/shader/static_model_diffuse.frag");

        s3d::Shader playerShader;
        playerShader.loadFromFile("data/shader/static_model_diffuse_player.vert", "data/shader/static_model_diffuse_player.frag");

        s3d::Shader roomShader;
        roomShader.loadFromFile("data/shader/room.vert", "data/shader/room.frag");

        s3d::Shader skyboxShader;
        skyboxShader.loadFromFile("data/shader/skybox.vert", "data/shader/skybox.frag");

        s3d::Material playerMaterial;
        playerMaterial.setShader(&playerShader);
        playerMaterial.setDiffuse(&playerTexture);

        s3d::Material gizmoMaterial;
        gizmoMaterial.setShader(&textureShader);
        gizmoMaterial.setDiffuse(&gizmoTexture);

        s3d::Material roomMaterial;
        roomMaterial.setShader(&roomShader);
        roomMaterial.setDiffuse(&roomTexture);

        s3d::Model room;
        room.setGeometry(&roomGeometry);
        room.setMaterial(&roomMaterial);
        room.transform()->scale(glm::vec3(100));
        room.transform()->setPosition(glm::vec3(0, room.getSize().y / 2, 0));
        scene.addModel(&room);

        s3d::Model gizmoModel;
        gizmoModel.setGeometry(&gizmo);
        gizmoModel.setMaterial(&gizmoMaterial);
        scene.addModel(&gizmoModel);

        s3d::Skybox skybox;
        skybox.setCubemap(&sky);
        skybox.setShader(&skyboxShader);
        scene.setSkybox(&skybox);

        // Game
        World gameWorld;
        gameWorld.setPlayerGeometry(&playerGeometry);
        gameWorld.setPlayerMaterial(&playerMaterial);
        gameWorld.setScene(&scene);

        gameWorld.score()->setFont(&bit8Operator);
        gameWorld.hud()->setTargetFont(&bit8Operator);
        gameWorld.hud()->setHealthFont(&bit8Operator);
        gameWorld.hud()->setNotificationFont(&bit8Operator);
        gameWorld.hud()->setNotification("Ace of Blades");

        PlayerInput playerInput;
        playerInput.setWorld(&gameWorld);

        // Networking
        unsigned short port = 80;
        if(!gClient.bindToPort(port)) {
            gClient.unbind();
            while(!gClient.bindToPort(rand() % 3000 + 500)) { }
            console.print("failed to bind to port 80, bound to " + Console::Variable::convertToString(gClient.getPort()) + " instead");
        }
        gClient.setConsole(&console);
        gClient.setWorld(&gameWorld);

        // Timers and input
        sf::Clock dtTimer;
        s3d::Input input;
        while(gRunning && gWindow.get()->isOpen()) {
            sf::Event event;
            while(gWindow.get()->pollEvent(event)) {
                if(event.type == sf::Event::LostFocus) {
                    s3d::Input::setGlobalEnabled(false);
                } else if(event.type == sf::Event::GainedFocus) {
                    s3d::Input::setGlobalEnabled(true);
                } else if(event.type == sf::Event::Closed) {
                    gRunning = false;
                }
            }
            if(gWindow.get()->isOpen()) {
                s3d::Input::update();

                const float dt = dtTimer.restart().asSeconds();
                const float fps = 1 / dt;
                tfps.setString("fps: " + s3d::util::toString((int)fps));

                gWindow.get()->setMouseCursorVisible(console.isVisible());
                playerInput.input()->setEnabled(!console.isVisible());
                gameWorld.score()->input()->setEnabled(!console.isVisible());

                // update local game state
                playerInput.update(&gWindow, &gCamera, dt);
                gameWorld.state()->update(dt);
                gameWorld.update(dt);
                gameWorld.score()->update();
                gameWorld.hud()->update();
                console.update(&event);

                // send local requests and get server updates
                gameWorld.state()->clearEvents();
                gClient.update();

                // render
                scene.render(&gWindow);
                gameWorld.render();
                gWindow.get()->pushGLStates();
                gWindow.get()->draw(tfps);
                gameWorld.score()->render(gWindow.get());
                gameWorld.hud()->render(gWindow.get());
                console.render(gWindow.get());
                gWindow.get()->display();
                gWindow.get()->popGLStates();
            }
        }
        gClient.disconnect();
    } catch(std::exception& e) {
        std::cout << e.what() << "\n";
        std::cout << "enter anything to continue\n";
        std::string nothing;
        std::cin >> nothing;
    }
    return 0;
}

int main() {
    int result = contextDependent();
    gWindow.get()->close();
    return result;
}
