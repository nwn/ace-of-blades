#ifndef AOB_SCOREBOARD_H
#define AOB_SCOREBOARD_H

#include "game/state.h"
#include "s3d/input.h"

#include <SFML/Graphics.hpp>

class World;
class Scoreboard {
public:
    Scoreboard();

    void update();
    void render(sf::RenderTarget* target);

    void setFont(sf::Font* font);
    sf::Font* getFont();
    const bool hasFont();

    void setWorld(World* world);
    World* getWorld();
    const bool hasWorld();

    s3d::Input* input();
private:
    bool mRender;

    s3d::Input mInput;
    sf::Font* mFont;
    World* mWorld;
    std::vector<std::string> mNames;
    std::string mTarget;
};

#endif // AOB_SCOREBOARD_H
