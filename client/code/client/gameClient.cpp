#include "gameClient.h"

GameClient::GameClient() {
    setTickrate(16);    // 64 times / second
    setTimeout(5000);

    setConsole(nullptr);
    setWorld(nullptr);
    mConnected = false;
}

void GameClient::connect(net::ClientPacket::Connect data) {
    net::ClientPacket packet;
    packet.mType = net::ClientPacket::Type::Connect;
    packet.mConnect = data;
    send(packet);
}
void GameClient::disconnect(net::ClientPacket::Disconnect data) {
    net::ClientPacket packet;
    packet.mType = net::ClientPacket::Type::Disconnect;
    packet.mDisconnect = data;
    send(packet);

    mConnected = false;
    if(hasWorld()) {
        getWorld()->reset();
        getWorld()->addLocalPlayers();
    }
}

const bool GameClient::connected() {
    return mConnected;
}

void GameClient::sendPacket() {
    net::ClientPacket packet;
    packet.mType = net::ClientPacket::Type::Update;
    if(hasWorld() && getWorld()->hasLocalPlayer()) {
        packet.mUpdate.mRequests.set(*getWorld()->getLocalPlayer()->requests());
    }
    send(packet);
}
void GameClient::handlePacket(sf::Packet& serverPacket) {
    net::ServerPacket packet;
    serverPacket >> packet;
    if(packet.mType == net::ServerPacket::Type::Connect) {
        mConnected = true;
        if(hasConsole()) {
            getConsole()->print("connected to " + getServerAddress().toString() + ":" + Console::Variable::convertToString(getServerPort()));

            if(hasWorld()) {
                getWorld()->reset();
            }
            // sync local players identity with the servers
            if(hasWorld()) {
                getWorld()->setLocalPlayerName(packet.mConnect.mName);
            }
        }
    } else if(packet.mType == net::ServerPacket::Type::Update) {
        if(!packet.mUpdate.mName.empty()) {
            if(hasWorld()) {
                getWorld()->setLocalPlayerName(packet.mUpdate.mName);
                if(hasConsole()) {
                    getConsole()->print("set name to '" + packet.mUpdate.mName + "'");
                }
            }
        } else if(!packet.mUpdate.mResponse.empty()) {
            // output the servers command response
            commandResponse(packet.mUpdate.mResponse);
        } else {
            // update our own world
            if(hasWorld()) {
                getWorld()->sync(packet.mUpdate.mState);
            }
        }
    } else if(packet.mType == net::ServerPacket::Type::Disconnect) {
        mConnected = false;

        if(hasConsole()) {
            getConsole()->print(getServerAddress().toString() + ":" + Console::Variable::convertToString(getServerPort()) + " disconnected: " + packet.mDisconnect.mReason);
        }
        if(hasWorld()) {
            getWorld()->reset();
            getWorld()->addLocalPlayers();
        }
    }
}
void GameClient::timeout() {
    if(mConnected) {
        net::ClientPacket::Disconnect packet;
        packet.mReason = "timeout";
        disconnect(packet);   // tell the server that we're disconnecting because he is too slow
        if(hasConsole()) {
            getConsole()->print(getServerAddress().toString() + ":" + Console::Variable::convertToString(getServerPort()) + " disconnected: timeout");
        }
    }
}

void GameClient::send(net::ClientPacket& clPacket) {
    sf::Packet packet;
    packet << clPacket;
    socket()->send(packet, getServerAddress(), getServerPort());
}

void GameClient::setConsole(Console* console) {
    mConsole = console;
}
Console* GameClient::getConsole() {
    return mConsole;
}
const bool GameClient::hasConsole() {
    return (getConsole() != nullptr);
}

void GameClient::setWorld(World* world) {
    mGameWorld = world;
}
World* GameClient::getWorld() {
    return mGameWorld;
}
const bool GameClient::hasWorld() {
    return (getWorld() != nullptr);
}

void GameClient::commandResponse(const std::string& response) {
    if(hasConsole()) {
        std::vector<std::string> outputs = Console::tokenize(response, '\n');
        for(int i=outputs.size()-1;i>=0;i--) {
            getConsole()->print(outputs.at(i));
        }
    }
}
