#include "client.h"

Client::Client() {
    socket()->setBlocking(false);
    setTickrate(10);
    setTimeout(2000);
}

const bool Client::bindToPort(const unsigned short port) {
    unbind();
    return (socket()->bind(port) == sf::Socket::Status::Done);
}
void Client::unbind() {
    socket()->unbind();
}
const unsigned short Client::getPort() {
    return socket()->getLocalPort();
}

void Client::update() {
    { // update and send
        if(mTickrateTimer.getElapsedTime().asMilliseconds() >= getTickrate()) {
            mTickrateTimer.restart();

            if(mTimeoutTimer.getElapsedTime().asMilliseconds() >= getTimeout()) {
                // tell the game that the server has timed out
                timeout();
            }
            // tell the game to send requests to server
            sendPacket();
        }
    }
    { // receive
        sf::Packet     foreignPacket;
        sf::IpAddress  foreignAddress;
        unsigned short foreignPort;

        sf::Socket::Status status = socket()->receive(foreignPacket, foreignAddress, foreignPort);
        if(status == sf::Socket::Done) {
            if(foreignAddress == getServerAddress() &&
               foreignPort    == getServerPort()) {
                mTimeoutTimer.restart();
                handlePacket(foreignPacket);
            }
        } else {
            if(status == sf::Socket::Status::Error) {
                // probably has not binded to a port yet
//                throw std::runtime_error("sf::Socket::Status::Error");
            }
        }
    }
}

void Client::setTickrate(const float tickrate) {
    mTickrate = tickrate;
}
void Client::setTimeout(const float timeout) {
    mTimeout = timeout;
}

const float Client::getTickrate() const {
    return mTickrate;
}
const float Client::getTimeout() const {
    return mTimeout;
}

void Client::setServerAddress(const sf::IpAddress& ipAddress) {
    mServerAddress = ipAddress;
}
void Client::setServerPort(const unsigned short port) {
    mServerPort = port;
}

sf::UdpSocket* Client::socket() {
    return &mSocket;
}

const sf::IpAddress& Client::getServerAddress() const {
    return mServerAddress;
}
const unsigned short Client::getServerPort() const {
    return mServerPort;
}
