#ifndef AOB_STATE_H
#define AOB_STATE_H

#include "player.h"

#include <console/console.h>
#include <s3dc/s3dc.h>
#include <vector>

namespace game {
class State {
public:
    State();

    void update(float dt);

    game::Player* addPlayer(const std::string& requestedName);
    const bool hasPlayer(const std::string& name);
    game::Player* getPlayer(const std::string& name);
    game::Player* getPlayer(const int i);
    const std::vector<game::Player*>& getPlayers();

    void removePlayer(const int i);
    void removePlayer(const std::string& name);
    void removeAllPlayers();

    void clearEvents();

    const std::string generateName(const std::string& requested);

    void set(game::State& state);

    const float getFriction();
    void setFriction(const float friction);

    const float getAirFriction();
    void setAirFriction(const float friction);

    const glm::vec3& getGravity();
    void setGravity(const glm::vec3& gravity);

    const float getRestitution();
    void setRestitution(const float restitution);
private:
    void updateMovement(game::Player* player, const float dt);
    void updateAttack(game::Player* player, const float dt);
    void updateDeath(game::Player* player, const float dt);
    void updatePhysics(game::Player* player, const float dt);

    std::vector<game::Player*> mPlayers;

    float mFriction;
    float mAirFriction;
    glm::vec3 mGravity;
    float mRestitution;
};
}

sf::Packet& operator << (sf::Packet& packet, game::State& m);
sf::Packet& operator >> (sf::Packet& packet, game::State& m);

#endif // AOB_STATE_H
