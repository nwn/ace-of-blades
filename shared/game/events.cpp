#include "events.h"

game::Events::Events() {
    clear();
}

void game::Events::set(Events& r) {
    mJumped = r.mJumped;
    mFired = r.mFired;
    mAltFired = r.mAltFired;
    mDied = r.mDied;
    mAssailant = r.mAssailant;
    mHurt = r.mHurt;
    mRespawned = r.mRespawned;
}
void game::Events::clear() {
    mJumped = false;
    mFired = false;
    mAltFired = false;
    mDied = false;
    mAssailant.clear();
    mHurt = false;
    mRespawned = false;
}

sf::Packet& operator << (sf::Packet& packet, game::Events& m) {
    packet << m.mJumped;
    packet << m.mFired;
    packet << m.mAltFired;
    packet << m.mDied;
    packet << m.mAssailant;
    packet << m.mHurt;
    packet << m.mRespawned;
    return packet;
}
sf::Packet& operator >> (sf::Packet& packet, game::Events& m) {
    packet >> m.mJumped;
    packet >> m.mFired;
    packet >> m.mAltFired;
    packet >> m.mDied;
    packet >> m.mAssailant;
    packet >> m.mHurt;
    packet >> m.mRespawned;
    return packet;
}
