#ifndef AOB_EVENTS_H
#define AOB_EVENTS_H

#include <SFML/Network.hpp>

namespace game {
class Events {
public:
    Events();

    void set(Events& r);
    void clear();

    bool mJumped;
    bool mFired;
    bool mAltFired;
    bool mDied;
    std::string mAssailant;
    bool mHurt;
    bool mRespawned;
};
}

sf::Packet& operator << (sf::Packet& packet, game::Events& m);
sf::Packet& operator >> (sf::Packet& packet, game::Events& m);

#endif // AOB_EVENTS_H
