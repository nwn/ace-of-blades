#ifndef AOB_WEAPON_H
#define AOB_WEAPON_H

#include "s3dc/s3dc.h"

#include <SFML/System/Clock.hpp>
#include <SFML/Network/Packet.hpp>

namespace game {
class Player;
class Weapon {
public:
    Weapon();
    virtual void fire(Player* const target) = 0;
    virtual void kickback() = 0;

    const bool ready() const;

    const float getDamage() const;
    const float getKickback() const;
    const float getCooldown() const;
    const float getCooldownTimer() const;

    void setDamage(const float damage);
    void setKickback(const float kickback);
    void setCooldown(const float cooldown);

    void restartCooldownTimer();

    void setOwner(Player* const owner);
    Player* const getOwner() const;
    const bool hasOwner();

protected:
    friend sf::Packet& operator << (sf::Packet& packet, game::Weapon& m);
    friend sf::Packet& operator >> (sf::Packet& packet, game::Weapon& m);

private:
    float mDamage;
    float mKickback;
    float mCooldown;
    sf::Clock mCooldownTimer;

    Player* mOwner;
};
class Raygun : public Weapon {
public:
    Raygun();
    void fire(Player* const target) override;
    void kickback() override;
};
sf::Packet& operator << (sf::Packet& packet, game::Weapon& m);
sf::Packet& operator >> (sf::Packet& packet, game::Weapon& m);
}

#endif // AOB_WEAPON_H
