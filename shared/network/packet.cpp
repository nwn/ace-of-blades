#include "packet.h"

sf::Packet& operator << (sf::Packet& packet, net::ServerPacket& m) {
    packet << (int)m.mType;

    if(m.mType == net::ServerPacket::Type::Connect) {
        packet << m.mConnect.mName;
    } else if(m.mType == net::ServerPacket::Type::Update) {
        packet << m.mUpdate.mName;
        packet << m.mUpdate.mResponse;
        packet << m.mUpdate.mState;
    } else if(m.mType == net::ServerPacket::Type::Disconnect) {
        packet << m.mDisconnect.mReason;
    }
    return packet;
}
sf::Packet& operator >> (sf::Packet& packet, net::ServerPacket& m) {
    int type = 0;
    packet >> type;
    m.mType = net::ServerPacket::Type(type);

    if(m.mType == net::ServerPacket::Type::Connect) {
        packet >> m.mConnect.mName;
    } else if(m.mType == net::ServerPacket::Type::Update) {
        packet >> m.mUpdate.mName;
        packet >> m.mUpdate.mResponse;
        packet >> m.mUpdate.mState;
    } else if(m.mType == net::ServerPacket::Type::Disconnect) {
        packet >> m.mDisconnect.mReason;
    }
    return packet;
}

sf::Packet& operator << (sf::Packet& packet, net::ClientPacket& m) {
    packet << (int)m.mType;

    if(m.mType == net::ClientPacket::Type::Connect) {
        packet << m.mConnect.mName;
    } else if(m.mType == net::ClientPacket::Type::Update) {
        packet << m.mUpdate.mName;
        packet << m.mUpdate.mCommand;
        packet << m.mUpdate.mRequests;
    } else if(m.mType == net::ClientPacket::Type::Disconnect) {
        packet << m.mDisconnect.mReason;
    }
    return packet;
}
sf::Packet& operator >> (sf::Packet& packet, net::ClientPacket& m) {
    int type = 0;
    packet >> type;
    m.mType = net::ClientPacket::Type(type);

    if(m.mType == net::ClientPacket::Type::Connect) {
        packet >> m.mConnect.mName;
    } else if(m.mType == net::ClientPacket::Type::Update) {
        packet >> m.mUpdate.mName;
        packet >> m.mUpdate.mCommand;
        packet >> m.mUpdate.mRequests;
    } else if(m.mType == net::ClientPacket::Type::Disconnect) {
        packet >> m.mDisconnect.mReason;
    }
    return packet;
}
