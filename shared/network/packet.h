#ifndef AOB_PACKET_H
#define AOB_PACKET_H

#include "../game/player.h"
#include "../game/requests.h"
#include "../game/state.h"
#include <SFML/Network.hpp>

namespace net {
class ServerPacket {
public:
    enum class Type {
        Connect,
        Update,
        Disconnect
    };
    struct Connect {
        std::string mName;
    };
    struct Update {
        std::string mName;
        std::string mResponse;
        game::State mState;
    };
    struct Disconnect {
        std::string mReason = "quit";
    };

    Type       mType;
    Connect    mConnect;
    Update     mUpdate;
    Disconnect mDisconnect;
};
class ClientPacket {
public:
    enum class Type {
        Connect,
        Update,
        Disconnect
    };
    struct Connect {
        std::string mName = "Player";
    };
    struct Update {
        std::string mName;
        std::string mCommand;
        game::Requests mRequests;
    };
    struct Disconnect {
        std::string mReason = "quit";
    };

    Type       mType;
    Connect    mConnect;
    Update     mUpdate;
    Disconnect mDisconnect;
};
}

sf::Packet& operator << (sf::Packet& packet, net::ServerPacket& m);
sf::Packet& operator >> (sf::Packet& packet, net::ServerPacket& m);

sf::Packet& operator << (sf::Packet& packet, net::ClientPacket& m);
sf::Packet& operator >> (sf::Packet& packet, net::ClientPacket& m);

#endif // AOB_PACKET_H
